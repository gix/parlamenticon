'use strict'
const merge = require('webpack-merge')
const customEnv = require('./custom.env')

module.exports = merge(customEnv, {
  NODE_ENV: '"production"'
})
