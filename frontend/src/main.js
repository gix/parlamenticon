// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import './stylus/main.styl'

// https://color.adobe.com/mike-kotsch-204198-unsplash-color-theme-10999842/?showPublished=true
Vue.use(Vuetify, {
  theme: {
    primary: '#5a8dab',
    secondary: '#b5d3db',
    accent: '#43617d',
    error: '#dad2c7'
  }
})

Vue.config.productionTip = false

let apiBaseURL = 'http://127.0.0.1:5000'
if (process.env.API_URI_BASE) {
  apiBaseURL = process.env.API_URI_BASE
}

let apiPrefix = '/api'
if (process.env.API_PREFIX) {
  apiPrefix = process.env.API_PREFIX
}

const axiosConfig = {
  baseURL: apiBaseURL + apiPrefix,
  timeout: 30000
}

Vue.prototype.$axios = axios.create(axiosConfig)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
