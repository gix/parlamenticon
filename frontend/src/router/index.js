import Vue from 'vue'
import Router from 'vue-router'
import Keywords from '@/components/Keywords'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {path: '/', redirect: '/parliament/house-of-deputies/2017/session/last/keywords'},
    {
      path: '/parliament/house-of-deputies/:p/session/:session/keywords',
      name: 'keywords',
      component: Keywords,
      props: true
    }
  ]
})
