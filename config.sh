#!/bin/bash

RUN_DIR=$(pwd)

if test "$#" -ne 1 ; then
  echo "error: illegal number of parameters"
  echo "usage: $0 [local|test|production]"
  exit
fi

DEPLOYMENT="$1"
DEPLOYMENT_DIR=

if [ "$DEPLOYMENT" = "local" ]
then
  DEPLOYMENT_DIR="./deployment/local"
fi

if [ "$DEPLOYMENT" = "test" ]
then
  DEPLOYMENT_DIR="./deployment/aws/test"
fi

if [ "$DEPLOYMENT" = "production" ]
then
  DEPLOYMENT_DIR="./deployment/aws/production"
fi


if [ -z "$DEPLOYMENT_DIR" ]
then
  echo "Can't set environment for deployment: $DEPLOYMENT."
  exit 1
fi

DOCKER_COMPOSE_FILE=$DEPLOYMENT_DIR/docker-compose-${DEPLOYMENT}.yaml
DOCKER_ENV_FILE=$DEPLOYMENT_DIR/env

ln -f -s "$DOCKER_COMPOSE_FILE" "docker-compose.yaml"
ln -f -s "$DOCKER_ENV_FILE" ".env"
