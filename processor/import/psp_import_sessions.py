#!/usr/bin/env python3
from datetime import datetime
from elasticsearch import Elasticsearch
import json
from collections import defaultdict, namedtuple
import sys
import os
import re
import datetime

dataset_id = 'psp'
dataset_prefix = "{}_".format(dataset_id)
elasticsearch_connection = os.getenv('ELASTICSEARCH_CONNECTION', 'http://localhost:9200')
es = Elasticsearch([elasticsearch_connection])
index_name = "{}{}sessions".format(dataset_prefix, os.getenv('ELASTICSEARCH_INDEX_PREFIX', ''))

if len(sys.argv) != 4:
    print("Error: not enough arguments.\nUsage: {} data_sid_to_stats_v2.json data_people_stats_by_sid_v2.json keywords.json", sys.argv[0])
    sys.exit(0)
forget, sid_stats_file, sid_people_stats_file, sid_keywords_file = sys.argv

print(sys.argv)
print("INDEX_NAME: ".format(index_name))
print("ELASTICSEARCH_CONNECTION: ".format(elasticsearch_connection))

with open(sid_people_stats_file, 'r') as f:
  session_people_stats = json.load(f)

with open(sid_stats_file, 'r') as f:
  session_stats = json.load(f)

with open(sid_keywords_file, 'r') as f:
  session_keywords = json.load(f)

try:
    print("ALERT: deleting old index if present {}".format(index_name))
    res = es.indices.delete(index=index_name)
    print(res)
except:
    print("Delete index error: it might not be present, passing.")


print("Creating mapping for index: {}".format(index_name))
mapping = '''
{
  "mappings":{
    "_doc": {
      "properties":{
        "created_at":{ "type":"date" },
        "dataset_id": {"type": "keyword"},
        "session_id": {"type": "integer"},
        "stats": {
          "type": "object",
          "properties": {
            "original_link": {"type": "text"},
            "length_days": {"type": "integer"},
            "date_from": {"type": "date"},
            "date_to": {"type": "date"},
            "word_count": {"type": "integer"},
            "utterance_count": {"type": "integer"},
            "std_word_count_per_utterance": {"type": "float"},
            "median_word_count_per_utterance": {"type": "float"},
            "mean_word_count_per_utterance": {"type": "float"},
            "unique_word_count": {"type": "integer"}
          }
        },
        "people_stats": {
          "type": "nested",
          "properties": {
            "role_predicates": {
              "type": "object",
              "properties": {
                "is_czech_president": {"type": "boolean"},
                "is_moderator": {"type": "boolean"},
                "is_minister": {"type": "boolean"},
                "is_guest": {"type": "boolean"},
                "is_czech_cabinet_member": {"type": "boolean"},
                "is_mp": {"type": "boolean"},
                "is_senator": {"type": "boolean"}
              }
            },
            "name": {"type": "keyword"},
            "role": {"type": "keyword"},
            "key": {"type": "keyword"},
            "utterance_count": {"type": "integer"},
            "mean_word_count_per_utterance": {"type": "float"},
            "median_word_count_per_utterance": {"type": "float"},
            "std_word_count_per_utterance": {"type": "float"},
            "word_count": {"type": "integer"},
            "unique_word_count": {"type": "integer"}
          }
        },
        "keywords": {
          "type": "nested",
          "properties": {
            "bg_count":  { "type": "integer" },
            "doc_count":  { "type": "integer" },
            "order":  { "type": "integer" },
            "people": { "type": "keyword" },
            "score": { "type": "float"  },
            "text": { "type": "text" },
            "keyword": { "type": "keyword" }
          }
        },
        "people_to_keywords": {
          "type": "nested",
          "properties": {
            "name": {"type": "keyword"},
            "keyword": {"type": "keyword"}
          }
        }
      }
    }
  }
}'''
print(mapping)
es.indices.create(index=index_name, body=mapping)

print("mapping for index {} created".format(index_name))

for psp_key in session_stats:
  for session_id in session_stats[psp_key]:
    doc_key = "{}_{}".format(re.sub("ps", "", psp_key), session_id)
    created_at = datetime.datetime.now().isoformat()
    session = { "dataset_id": re.sub("ps", "", psp_key), "session_id": session_id, "created_at": created_at}
    session['stats'] = session_stats[psp_key][session_id]
    print("Constructing data for {}.".format(doc_key))
    #print(session_stats)
    if ((psp_key in session_keywords) and (session_id in session_keywords[psp_key])):
      print(session_keywords[psp_key][session_id].keys())
      if 'keywords' in session_keywords[psp_key][session_id]:
        session['keywords'] = session_keywords[psp_key][session_id]['keywords']
        print("Keywords found.")
      if 'people_to_keywords' in session_keywords[psp_key][session_id]:
        session['people_to_keywords'] = session_keywords[psp_key][session_id]['people_to_keywords']
        print("PeopleToKeywords found.")

    if ((psp_key in session_people_stats) and (session_id in session_people_stats[psp_key])):
      session['people_stats'] = session_people_stats[psp_key][session_id]
      for person_stat in session['people_stats']:
        person_stat['key'] = "{}-{}".format(person_stat['name'], person_stat['role'])
      print("Session people stats found.")

    print("Constructing data for session: {} ({})".format(session_id, psp_key))
    doc = json.dumps(session)
    res = es.index(index=index_name, doc_type='_doc', id=doc_key, body=doc)

