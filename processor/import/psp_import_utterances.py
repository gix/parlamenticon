from datetime import datetime
from elasticsearch import Elasticsearch
import json
from collections import defaultdict, namedtuple
import sys
import os
import re

Row = namedtuple("Row", ['sid', 'order', 'date', 'role', 'name', 'text', 'lemmas', 'pos_tags', 'tokens'])
allowed_beginnings = ["N", "V", "A"]

dataset_id = 'psp'
dataset_prefix = "{}_".format(dataset_id)
elasticsearch_connection = os.getenv('ELASTICSEARCH_CONNECTION', 'http://localhost:9200')
es = Elasticsearch([elasticsearch_connection])
index_name = "{}{}utterances".format(dataset_prefix, os.getenv('ELASTICSEARCH_INDEX_PREFIX', ''))

print(sys.argv)
print("INDEX_NAME: {}".format(index_name))
print("ELASTICSEARCH_CONNECTION: {}".format(elasticsearch_connection))

if len(sys.argv) != 2:
    print("Usage: {} file_name.jsonl", sys.argv[0])
    sys.exit(0)

in_file = sys.argv[1]

rows = []
with open(in_file, 'r') as fin:
    for i, row_js in enumerate(fin):
        rowd = json.loads(row_js.strip())
        pos = 0
        text_tokens = []
        text_pos_tags = []
        text_lemmas = []
        for pos_tag in rowd['pos_tags']:
            lemma = rowd['lemmas'][pos]
            token = rowd['tokens'][pos]
            pos_tag = rowd['pos_tags'][pos]
            pos += 1
            if True:
                text_tokens.append(token)
                text_lemmas.append(lemma)
                text_pos_tags.append(pos_tag)
        rowd["orig_text"] = rowd["text"]
        rowd["text"] = " ".join(text_lemmas)
        rowd["pos_tags"] = text_pos_tags
        rowd["tokens"] = text_tokens
        rowd["lemmas"] = text_lemmas
        rows.append(rowd)

print('example data')
print(rows[0])

try:
    print('ALERT: deleting old index if present')
    res = es.indices.delete(index=index_name)
    print(res)
except:
    print("Unexpected error:")


print("creating mapping...")
mapping = '''
{
  "mappings":{
    "doc":{
      "properties":{
        "name":{
          "type":"keyword"
        },
        "role":{
          "type":"keyword"
        },
        "lemmas":{
          "type":"keyword"
        },
        "pos_tags":{
          "type":"keyword"
        },
        "tokens":{
          "type":"keyword"
        },
        "date":{
          "type":"keyword"
        }
      }
    }
  }
}'''
es.indices.create(index=index_name, ignore=400, body=mapping)


print('importing data ...')
id = 0
for row in rows:
    batch =  row
    doc = json.dumps(batch)
    res = es.index(index=index_name, doc_type='doc', id=id, body=doc)
    #print('.')
    id += 1

es.indices.refresh(index=index_name)
