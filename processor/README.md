#JAK TESTOVAT PROCESSOR

## I - Lokální testování v command line:
### I.1 - Instalace závislostí
- `virtualenv  -p /usr/bin/python3 /tmp/venv/`
- `source /tmp/venv/bin/activate`
- `cd ./preprocessor && pip3 install -r requirements.txt`
### I.2 - Běh celé pipeline
- `make`
### I.2b - Běh části pipeline
Je možné přeskočit část pipeline, pokud víte (!), že už jsou na odpovídajících místech data.

- 1) přeskoč scraping www.psp.cz: `make SCRAPE_PSP=n`
- 2) přeskoč scraping a preprocessing senátu: `make SCRAPE_N_PARSE_S=n`
- 3) přeskoč import utterances: `make IMPORT_PSP_UTTERANCES=n`
- 4) přeskoč import sessions `make IMPORT_PSP_SESSIONS=n`

Nyní je časově nejnáročnějším krokem 3) import utterances a 1) scrapování psp.
V dockerech to zatím použít neumím, souvisí to s CMD, ENTRYPOINT a parametry pro `docker-compose run` či `docker run`.

## II Lokální testování v dockerech:
- `cd parlamenticon`
- `./config.sh local`
- `docker-compose build`
- `docker-compose up local_processor`
- `docker-compose up -d local_frontend && docker-compose up -d local_api`

Původně jsem používala `docker-compose run local_processor`, ale nechová se to tak, jak jsem se domnívala. Je potřeba zjistit, jak správně připojit efemérní disky processsoru jako parametr docker run.
