#!/bin/bash

if test "$#" -ne 1 ; then
  echo "error: illegal number of parameters"
  echo "usage: $0  [production|local|test]"
  exit
fi

DEPLOYMENT=$1
if [ "$DEPLOYMENT" == "production" ] || [ "$DEPLOYMENT" == "local" ] || [ "$DEPLOYMENT" == "test"]
then
  echo "Running processor in deployment: $DEPLOYMENT"
  echo "Note: WILL restart API if successful."
  docker-compose run "$DEPLOYMENT"_processor && docker-compose stop "$DEPLOYMENT"_api && docker-compose up -d "$DEPLOYMENT"_api || (echo "Process ended unsuccessfully"; exit 0)
  echo "Successfully restarted API in deployment: $DEPLOYMENT"
else
  echo "Unknown deployment parameter: $DEPLOYMENT, can't run processor"
fi


