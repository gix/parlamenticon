#!/bin/bash

if test "$#" -ne 1 ; then 
  echo "error: illegal number of parameters"
  echo "usage: $0  [production|local|test]"
  exit
fi

PARLAMENTICON_HOME=$HOME/parlamenticon

DEPLOYMENT=$1
# run five minutes after midnight, every day
CRONTAB_JOB_STRING="5 0 * * * cd $PARLAMENTICON_HOME && ./processor/utils/run_processor.sh $DEPLOYMENT"

if [ "$DEPLOYMENT" == "production" ] || [ "$DEPLOYMENT" == "local" ] || [ "$DEPLOYMENT" == "test"]
then
  echo "$CRONTAB_JOB_STRING"
  crontab -l | grep -q "run_processor.sh $DEPLOYMENT"  && echo 'Entry exists in crontab, please check manually via `crontab -e`, exiting.' && exit 0

  echo "Initiated installation of processor in crontab for deployment: $DEPLOYMENT."
  echo "Processor in crontab not found for deployment: $DEPLOYMENT."
  (crontab -l 2>/dev/null; echo "$CRONTAB_JOB_STRING") | crontab -
  echo "Job ($DEPLOYMENT): $CRONTAB_JOB_STRING"
  echo "Successfully installed processor in crontab for deployment: $DEPLOYMENT."
else
  echo "Unknown deployment parameter: $DEPLOYMENT, installing crontab failed."
fi


