#!/bin/bash

[ -z "$1" -o -z "$2" ] && {
cat << EOF
usage: $0 DATA_DIR TARGET_LINK_FILE
downloads senat stenorecords, saves to timestamped files under
DATA_DIR/www.senat.cz/
parses the files and stores result into file like
DATA_DIR/senat_2018-07-29.jsonl
finally, creates/updates a symlink TARGET_LINK_FILE to point to
this jsonl file.

arguments
    DATA_DIR
    directory where to store the downloaded files

    TARGET_LINK_FILE
    updates/creates this link file to point out to the resulting jsonl file
EOF
exit 1
}

DATA_DIR="$1"
LINK_FILE="$2"

DOWN_DIR="$DATA_DIR/www.senat.cz"
mkdir -p "$DOWN_DIR"

OUTPUT_FILE_JSONL="${DATA_DIR}/senat_$(date "+%Y-%m-%d").jsonl"

# load scrape utils
SCRIPT_HOME=$(dirname "$0")
. "$SCRIPT_HOME/scrape_utils.sh"

SENAT_INDEX=$(mktemp)
wget_it 'https://www.senat.cz/xqw/xervlet/pssenat/finddoc?typdok=steno' -O "$SENAT_INDEX"

"$SCRIPT_HOME/parse_senat_root.py" "$SENAT_INDEX" | while read obdobi schuze url ; do
FILENAME="$(echo "$url" | sed 's#.*/##') $obdobi $schuze"
wget_it "$url" -O "${DOWN_DIR}/$FILENAME"
done

ls "$DOWN_DIR" | sort -n | while read filename ; do
read basename obdobi schuze <<< "$filename"
"$SCRIPT_HOME/parse_senat_one.py" "senat$obdobi" "$schuze" "${DOWN_DIR}/$filename"
done > "$OUTPUT_FILE_JSONL"

"$SCRIPT_HOME/update_link.sh" "$LINK_FILE" "$OUTPUT_FILE_JSONL"

rm -f "$SENAT_INDEX"
