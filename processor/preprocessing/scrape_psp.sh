#!/bin/sh

[ -z "$1" -o -z "$2" ] && {
cat << EOF
usage: $0 DATA_DIR PS_ROOT [PS_ROOT ...]
downloads archives of parliament stenorecords for the given
PS_ROOTS

arguments
    DATA_DIR
    where to download the files

    PS_ROOT
    the root to download, such as '2017ps'
    by default, the wget waits 0.2 secs between requests
    and only downloads file if it is newer on server
    (or missing locally)

example
    # downloads whole archives for
    # the last two "voting periods" (as of Jun 2018)
    $0 2017ps 2013ps
EOF
exit 1
}

DATA_DIR="$1"
shift 1

# load scrape utils
. $(dirname "$0")/scrape_utils.sh

cd "$DATA_DIR"
for arg in "$@" ; do
    wget_it -N -r -l 3 -np "https://www.psp.cz/eknih/$arg/stenprot/"
done

exit 0
