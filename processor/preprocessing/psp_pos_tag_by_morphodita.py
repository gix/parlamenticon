# This file is part of MorphoDiTa <http://github.com/ufal/morphodita/>.
#
# Copyright 2015 Institute of Formal and Applied Linguistics, Faculty of
# Mathematics and Physics, Charles University in Prague, Czech Republic.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys

import re

import glob

from ufal.morphodita import *

import json
from collections import defaultdict, namedtuple
import sys

Row = namedtuple("Row", ['sid', 'order', 'date', 'role', 'name', 'text'])

def encode_entities(text):
  return text.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;')

if len(sys.argv) != 4:
  sys.stderr.write('Usage: %s tagger_file in_dir\n' % sys.argv[0])
  sys.stderr.write('Example: python3 morphodita_run_tagger.py models/czech-morfflex-pdt-161115.tagger  in_data.jsonl  out_data.jsonl')
  sys.stderr.write("\n")
  sys.exit(1)

tagger = Tagger.load(sys.argv[1])
tokenizer = tagger.newTokenizer()
if tokenizer is None:
  sys.stderr.write("No tokenizer is defined for the supplied model!")
  sys.exit(1)

forms = Forms()
lemmas = TaggedLemmas()
tokens = TokenRanges()

in_file = sys.argv[2]
out_file = sys.argv[3]

of = open(out_file, 'w', encoding="utf-8")

rows = []
row_number = 1
with open(in_file, 'r') as fin:
    for i, row_js in enumerate(fin):
        rowd = json.loads(row_js.strip())
        text = rowd['text']
        rowd["tokens"] = []
        rowd["pos_tags"] = []
        rowd["lemmas"] = []
        tokenizer.setText(text)
        while tokenizer.nextSentence(forms, tokens):
          tagger.tag(forms, lemmas)

          for i in range(len(lemmas)):
            lemma = lemmas[i]
            lemma_text = re.sub("_(.*)?$", "", lemma.lemma)
            lemma_text = lemma_text.lower()
            token = tokens[i]
            token_string = text[token.start : token.start + token.length]
            rowd["tokens"].append(token_string)
            rowd["lemmas"].append(lemma_text)
            rowd["pos_tags"].append(lemma.tag)
        json.dump(rowd, of)
        of.write('\n')
        
        #if row_number == 2:
        #    break

