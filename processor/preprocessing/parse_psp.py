#!/usr/bin/env python3
import json
import os
import re
import sys

from joblib import Parallel, delayed

from parse_utils import load_soup, stderr, parse_speaker, tag2str, Row


def iter_speaker_a(body):
    for a in body.find_all("a"):
        if re.match(r'^r[0-9]*$', a.attrs.get('id', '')):
            yield a


def get_date(body):
    date_tag = body.find("p", class_='date')
    if date_tag is None:
        return
    return date_tag.string.extract()


def has_speaker(t, speakers):
    c = t.findChildren()
    for s in speakers:
        if s in c:
            return s

    return None


months_all = """led únor břez dub květ června července srp zář říj list prosin"""
mo_beg = months_all.split()
assert len(mo_beg) == 12


def parse_date(date):
    toks = date.split()
    assert len(toks) == 4
    raw_day, raw_mo, raw_year = toks[1:]

    dt = raw_day.split('.')
    assert len(dt) == 2
    assert dt[1] == ""
    day = int(dt[0])
    assert 0 < day <= 31

    mo = None
    for i, prefix in enumerate(mo_beg):
        if raw_mo.startswith(prefix):
            mo = i + 1
            break
    assert not mo is None
    assert 1 <= mo <= 12

    year = int(raw_year)
    assert 2040 >= year >= 2000

    return "%d-%02d-%02d" % (year, mo, day)


def process_one_file(filename):
    if not os.path.exists(filename):
        stderr("file does not exist, skipping: '%s'" % filename)
        return None

    basename = os.path.basename(filename).split('.')[0]
    soup = load_soup(filename)
    body = soup.find("div", id='body')
    if not body:
        stderr("could not find body, skipping: '%s'" % filename)
        return None

    date_str = get_date(body)
    if not date_str:
        stderr("could not find date, skipping: '%s'" % filename)
        return None

    date = parse_date(date_str)
    speakers = list(iter_speaker_a(body))

    current_speaker = None
    raw_rows = []
    ps = body.find_all('p', align='justify')
    for p in ps:
        new_speaker = has_speaker(p, speakers)
        if new_speaker:
            current_speaker = new_speaker

        text = tag2str(p)
        if text:
            raw_rows.append((parse_speaker(tag2str(current_speaker), basename), text))

    return basename, date, raw_rows



if __name__ == "__main__":
    assert len(sys.argv) >= 4
    key = sys.argv[1]
    sid = int(sys.argv[2])

    results = Parallel(n_jobs=3)(delayed(process_one_file)(filename) for filename in sys.argv[3:])

    previous_speaker = None
    count = 0
    for ret in results:
        if ret is None:
            continue

        i = 0
        basename, date, raw_rows = ret
        for speaker, text in raw_rows:
            if speaker is None:
                speaker = previous_speaker
            previous_speaker = speaker

            if not speaker:
                stderr("%s: skipping row without speaker; text='%s'" % (basename, text))
                continue

            name, title = speaker
            row = Row(key, str(sid), basename, i + 1, date, title, name, text)

            print(json.dumps(row._asdict()))
            count += 1
            i += 1

    stderr("%s s%03d: written %d rows" % (key, sid, count))
