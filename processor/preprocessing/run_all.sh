#!/bin/sh

[ -z "$1" ] && {
cat << EOF
usage: $0 DATA_DIR [PS_ROOT...]
downloads senat & psp & runs parsing of both

arguments
    DATA_DIR
    directory where to store both the downloaded files
    and the processed files; most-importantly, links such as
    senat.jsonl
    2017ps.jsonl
    are created to point to the most current output file

    PS_ROOTs
    if empty, 2017ps is used
EOF
exit 1
}

DATA_DIR="$1"
shift 1
PS_ROOT="$@"
[ -z "$PS_ROOT" ] && PS_ROOT='2017ps'

export SCRIPT_HOME=$(dirname "$0")

# SENAT
"$SCRIPT_HOME/scrape_n_parse_senat.sh" "$DATA_DIR" "$DATA_DIR/senat.jsonl"

# PSP
"$SCRIPT_HOME/scrape_psp.sh" "$DATA_DIR" $PS_ROOT
"$SCRIPT_HOME/parse_psp.sh" "$DATA_DIR" $PS_ROOT

# PARTY MEMBERSHIP
"$SCRIPT_HOME/scrape_n_parse_parties.sh" "$DATA_DIR"
