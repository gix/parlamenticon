#!/bin/sh
DATA_DIR="$1"
[ -z "$DATA_DIR" ] && {
cat << EOF
usage: $0 DATA_DIR

Scrapes and parses
https://www.psp.cz/sqw/organy2.sqw?k=1
to get a list of all members of parliament along with their party
membership, participation in "vybor", etc.

Outputs 2 files:
DATA_DIR/psp_list_of_mps.jsonl
    each line is a json dict of a member of a parliament. E.g.
    {
        "jmeno": "V\u011bra Ad\u00e1mkov\u00e1",
        "plne_jmeno": "prof. MUDr.\u00a0V\u011bra\u00a0Ad\u00e1mkov\u00e1,\u00a0CSc.",
        "volebni_kraj": "Hlavn\u00ed m\u011bsto Praha",
        "klub": "ANO",
        "vybory": ["VZ"],
        "komise": [],
        "delegace": []
    }
DATA_DIR/psp_sqw_index.json
    index from all values in previous file (such as "ANO", "VZ", "Hlavn\u00ed m\u011bsto Praha")
    to sqw index of the PSP database.
    E.g. for:
    "volebni_kraj": "Olomouck\u00fd"
    we have
    "Olomouck\u00fd": "592"
    Each index is unique and can be used for joins (just like tokens can)
    and for getting link with details, such as:
    https://www.psp.cz/sqw/snem.sqw?id=592
    which contains a list of all mps from olomoucky kraj.
EOF
exit 1
}

SCRIPT_HOME=$(dirname "$0")
. "$SCRIPT_HOME/scrape_utils.sh"

URL_ROOT='https://www.psp.cz/sqw/'

OUT_PARTIES="${DATA_DIR}/psp_list_of_mps.jsonl"
OUT_INDEX="${DATA_DIR}/psp_sqw_index.json"

ALL_FILES=$(mktemp)

#cat 'organy2.sqw?k=1' \
wget_it  "$URL_ROOT"'organy2.sqw?k=1' -O - \
        | sed -n '/<!-- Body -->/,/<!-- Menu -->/s/>/>\n/gp;' \
        | grep 'snem.sqw' \
        | sed 's#.*HREF=#'"$URL_ROOT"'#;s/>$//' \
        | while read url ; do
TMP_FILE=$(mktemp)
# XXX
# We need to fix the page to be a valid html
# I say a big WTF to whoever coded the web
wget_it -O - "$url" | sed 's/<sqw_flush>/\n/g;s/<tr>[[:space:]]*<th>/<tr><td>/g' > "$TMP_FILE"
echo "$TMP_FILE"
done > "$ALL_FILES"

cat "$ALL_FILES" | xargs "$SCRIPT_HOME"/parse_parties_one.py "$OUT_PARTIES" "$OUT_INDEX"

cat "$ALL_FILES" | rm -f
rm "$ALL_FILES"
