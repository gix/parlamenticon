#!/bin/sh

USER_AGENT="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101"

wget_it () {
    wget -nv --wait 0.2 --random-wait -e robots=off "--user-agent=$USER_AGENT" "$@"
}
