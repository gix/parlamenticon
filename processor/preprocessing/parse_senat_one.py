#!/usr/bin/env python3
import json
import re
import sys
from collections import OrderedDict
import bs4

from parse_utils import parse_speaker, polish, Row, load_soup, stderr


def parse_row(row):
    d = OrderedDict(
        [
            ('id_obdobi', row.find('id_obdobi').string),
            ('cislo_schuze', row.find('cislo_schuze').a.string),
            ('organ', row.find('zkratka_organu').div.string),
            ('tdocname', row.find('tdocname').string),
            ('datum', row.find('datum').string),
            ('size', row.find('size').string),
            ('zkratka_organu', row.find('zkratka_organu').div.string),
            ('original', row.find('original').a.get('href'))
        ]
    )
    return d


skip_classes_fullmatch = set(("neautorizovano", 'stenotisk', 'stenopoznamka'))
skip_classes_start = ("audio", 'video')


def is_skippable(cl):
    if cl in skip_classes_fullmatch:
        return True
    for prefix in skip_classes_start:
        if cl.startswith(prefix):
            return True

    return False


def nadpis2date(tag):
    nadpis = tag.string.extract().strip()
    nadpis = re.sub(r'[()]', r'', nadpis)
    date = nadpis.split()[-1]

    day, mo, year = tuple(map(int, date.split('.')))

    return "%d-%02d-%02d" % (year, mo, day)


if __name__ == "__main__":
    key, sid, *files = sys.argv[1:]
    sid = int(sid)

    for filename in files:
        soup = load_soup(filename)
        body = soup.find('body')
        date = None
        previous_speaker = None
        count = 0
        skipped = 0
        for n, tag in enumerate(body.children):
            if not isinstance(tag, bs4.element.Tag):
                continue
            if tag.name not in ['p', 'div']:
                continue
            classes = set(tag.get('class', []))
            if any(map(is_skippable, classes)):
                skipped += 1
                continue

            assert len(classes) == 1

            if 'stenonadpis' in classes:
                date = nadpis2date(tag)
            elif 'stenotitul' in classes:
                title = polish(tag.text)
                count += 1
                row = Row(key, str(sid), filename, count, date, '', '', title)
            elif 'stenovystoupeni' in classes:
                assert tag.name == 'div'

                svyst = tag.find(class_="stenovystupujici")
                speaker = previous_speaker

                if svyst is not None:
                    ss = svyst.text
                    ss = re.sub(r':$', r'', ss)
                    speaker = parse_speaker(ss)
                    previous_speaker = speaker

                allps = tag.find_all('p')
                ptexts = [p.text.strip() for p in allps if p is not None]
                txt1 = polish(tag.text)
                txt2 = polish(' '.join(ptexts))
                # txt1 sometimes misses space between sentences,
                # but on the other hand it is less likely to miss
                # some text (because txt2 is only built from <p> tags)
                assert len(txt2) >= len(txt1)
                assert re.sub(r'\s', r'', txt1) == re.sub(r'\s', r'', txt2)
                assert date != None

                for text in ptexts:
                    name, title = speaker
                    assert title != None
                    assert name != None
                    count += 1
                    row = Row(key, str(sid), filename, count, date, title, name, text)
                    print(json.dumps(row._asdict()))

        stderr("'%s': written %4d rows; skipped %4d rows" % (filename, count, skipped))
