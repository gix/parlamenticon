#!/bin/sh

[ -z "$1" -o -z "$2" ] && {
cat << EOF
usage: $0 DATA_DIR PS_ROOT [PS_ROOT ...]
parses downloaded archives of stenorecords, saves to timestamped files

arguments
    DATA_DIR
    directory containing parsed data

    PS_ROOT
    the root to parse, such as '2017ps'

example
    # parses the whole archives for
    # the last two "voting periods" (as of Jun 2018)
    $0 data 2017ps 2013ps
    # the result are written to timestamped FILES
EOF
exit 1
}

data_dir="$1"
shift
echo "data_dir: ${data_dir}" >&2

out_tail="$(date "+%Y-%m-%d").jsonl"

for ps in "$@"
do
    out_file="${data_dir}/${ps}_${out_tail}"
    link_file="${data_dir}/${ps}.jsonl"

    for dir in  "${data_dir}/www.psp.cz/eknih/${ps}/stenprot/"[0-9]*schuz ; do
        schuz=$(basename "$dir" | sed 's/^\([0-9]*\)schuz/\1/')
        $(dirname "$0")/parse_psp.py "$ps" "$schuz"  "${dir}/s${schuz}"*
    done > "$out_file"

    $(dirname "$0")/update_link.sh "$link_file" "$out_file"
done
