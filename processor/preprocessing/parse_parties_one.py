#!/usr/bin/env python3
import json
import re
import sys
from collections import namedtuple

from parse_utils import load_soup

field_names = ('Jméno', 'Volební kraj ', 'Klub', 'Výbor(y)', 'Komise', 'Delegace')
Fields = namedtuple("Fields", ['jmeno', 'plne_jmeno', 'volebni_kraj',
                               'klub', 'vybory', 'komise', 'delegace'])


def parse_list(field):
    l = field.split(', ')
    return [one for one in l if one]


def td2ids(td):
    return [re.sub(r'.*=', r'', a.get('href')) for a in td.find_all('a')]


def parse_row(tds):
    assert len(tds) == 6

    ids = [td2ids(td) for td in tds]
    lens = [len(id) for id in ids]
    fields = [td.text.strip() for td in tds]
    assert lens[0:3] == [1, 1, 1]

    name_nbsp = tds[0].find('a').text
    name = re.sub(r'\u00a0', r' ', name_nbsp)
    name_full, kraj, klub = fields[0:3]
    d = {name: ids[0][0],
         name_nbsp: ids[0][0],
         name_full: ids[0][0],
         kraj: ids[1][0],
         klub: ids[2][0],
         }

    lid = [3, 4, 5]
    lists = [parse_list(fields[i]) for i in lid]
    vybory, komise, delegace = lists
    for n, i in enumerate(lid):
        assert lens[i] == len(lists[n])
        for id, val in zip(ids[i], lists[n]):
            d[val] = id

    return d, Fields(
        name, name_full, kraj, klub,
        vybory, komise, delegace
    )


if __name__ == "__main__":
    out_jsonl, out_dict, *files = sys.argv[1:]

    D = {}
    with open(out_jsonl, 'w') as fout:
        for filename in files:
            soup = load_soup(filename)
            body = soup.find('div', id='body')
            table = body.find('table')
            assert table is not None
            trs = table.find_all('tr')
            assert len(trs) >= 1
            headers = trs[0].find_all('th')
            assert field_names == tuple(th.text for th in headers)

            for tr in trs[1:]:
                d, row = parse_row(tr.find_all('td'))
                fout.write(json.dumps(row._asdict()) + "\n")

                for k, v in d.items():
                    if k in D:
                        assert D[k] == v
                D.update(d)

    with open(out_dict, 'w') as fout:
        json.dump(D, fout)
