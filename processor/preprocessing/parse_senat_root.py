#!/usr/bin/env python3
import sys
from collections import OrderedDict

from parse_utils import load_soup, stderr


def parse_row(row):
    link = row.find('original').a.get('href')
    if not link.startswith("http"):
        assert link.startswith('/')
        link = "https://www.senat.cz%s"%link

    d = OrderedDict(
        [
            ('id_obdobi', row.find('id_obdobi').string),
            ('cislo_schuze', row.find('cislo_schuze').text),
            ('organ', row.find('zkratka_organu').div.string),
            ('tdocname', row.find('tdocname').string),
            ('datum', row.find('datum').string),
            ('size', row.find('size').string),
            ('zkratka_organu', row.find('zkratka_organu').div.string),
            ('original', link)
        ]
    )
    return d


def is_skippable(row):
    for key, val in [('tdocname', 'steno'),
                     ('organ', 'Senát')]:
        if row[key] != val:
            return True, (key, val, row[key])
    return False, ()


if __name__ == "__main__":
    for filename in sys.argv[1:]:
        soup = load_soup(filename)
        body = soup.find('div', id="container")
        rows = body.find_all('tr', class_='body')
        for n, raw in enumerate(rows):
            row = parse_row(raw)
            id = "%s %s %s" % (row['id_obdobi'], row['cislo_schuze'], row['original'])

            skip_flag, reason = is_skippable(row)
            if skip_flag:
                key, expect, got = reason
                stderr("'%s' != '%s' but '%s', skipping; '%s'"%(key, expect, got, id))
                continue

            print(id)
