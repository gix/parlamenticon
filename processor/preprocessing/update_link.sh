#!/bin/sh

[ -z "$1" -o -z "$2" ] && {
cat <<EOF
usage: $0 LINK_NAME NEWFILE

checks for differences between the two input files; if the new file
is different creates patch to get from old to new, and updates
the LINK_NAME (which is a symlink name) to the new file.
EOF
exit 1
}

LINK_NAME="$1"
NEWFILE="$2"

diff "$LINK_NAME" "$NEWFILE" > /dev/null 2>&1
[ "$?" -eq 0 ] || {
    [ -e "$LINK_NAME" -a '!' -h "$LINK_NAME" ] && {
        echo "WARN: $LINK_NAME exists, but is not a symbolic link" >&2
        exit 1
    }
    rm -f "$LINK_NAME"

    echo "updating $LINK_NAME to be $NEWFILE" >&2
    ln -s "$NEWFILE" "$LINK_NAME"
}