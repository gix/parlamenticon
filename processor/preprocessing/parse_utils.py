import re
import sys
from collections import namedtuple

from bs4 import BeautifulSoup
from gensim.utils import deaccent


def load_soup(filename):
    data = open(filename, 'r', encoding='cp1250').read()
    return BeautifulSoup(data, 'html.parser')

def stderr(*args, **kwargs):
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)


def polish(text):
    text = text.strip()
    text = re.sub(r'\n',r' ', text)
    text = re.sub(r'[ ]+', r' ', text)
    return text


def normalize(word):
    word = word.strip().lower()
    return deaccent(word)


fix_len_roles = {
    1: [
        'senátor',
        'senátorka',
        'poslanec',
        'poslankyně',
        'pan',
        'paní',
    ],
    2: [
        'guvernér čnb',
        'guvernérka čnb',
        'místopředseda psp',
        'místopředsedkyně psp',
        'předseda psp',
        'předsedkyně psp',
        'předseda senátu',
        'předsedkyně senátu',
        'místopředseda senátu',
        'místopředsedkyně senátu'
    ],
    3: [
        '1. místopředseda senátu',
        '1. místopředsedkyně senátu'
    ]
}

# deacc & lower case
# from corpus & PS 2017 & PS 2013
given_names = {'adam', 'adolf', 'alena', 'ales', 'alexander', 'alexandr', 'alexandra', 'alice', 'alois', 'andrej',
               'anezka', 'anna', 'anton', 'antonin', 'arnost', 'augustin', 'barbora', 'bedrich', 'blanka', 'blazena',
               'bohumil', 'bohumila', 'bohumir', 'bohuslav', 'bohuslava', 'boris', 'bozena', 'bretislav', 'bronislav',
               'bronislava', 'cestmir', 'dagmar', 'dalibor', 'dan', 'dana', 'daniel', 'daniela', 'danuse', 'darina',
               'dasa', 'david', 'dominik', 'drahomir', 'drahomira', 'drahoslava', 'dusan', 'edita', 'eduard', 'elena',
               'eliska', 'emil', 'emilia', 'emilie', 'erika', 'eva', 'evzen', 'frantisek', 'frantiska', 'gabriela',
               'hana', 'helena', 'herbert', 'igor', 'ilja', 'ilona', 'irena', 'iva', 'ivan', 'ivana', 'iveta', 'ivo',
               'jakub', 'jan', 'jana', 'jarmila', 'jaromir', 'jaroslav', 'jaroslava', 'jeronym', 'jindra', 'jindrich',
               'jindriska', 'jiri', 'jirina', 'jitka', 'josef', 'jozef', 'julius', 'juraj', 'kamal', 'kamil', 'kamila',
               'karel', 'karla', 'katarina', 'katerina', 'klara', 'kristina', 'kristyna', 'kveta', 'kvetoslav',
               'kvetoslava', 'kvetuse', 'ladislav', 'ladislava', 'lenka', 'leo', 'leos', 'libor', 'libuse', 'lubomir',
               'lubos', 'lucie', 'ludek', 'ludmila', 'ludvik', 'lukas', 'lumir', 'lydie', 'magda', 'magdalena',
               'marcel', 'marcela', 'marek', 'margita', 'maria', 'marian', 'marie', 'marketa', 'marta', 'martin',
               'martina', 'matej', 'michael', 'michaela', 'michal', 'mikulas', 'milada', 'milan', 'milena', 'milos',
               'miloslav', 'miloslava', 'miluse', 'miroslav', 'miroslava', 'mojmir', 'monika', 'nadezda', 'natasa',
               'nina', 'oldrich', 'olga', 'ondrej', 'otakar', 'oto', 'otto', 'patrik', 'paul', 'pavel', 'pavla',
               'pavlina', 'pavol', 'peter', 'petr', 'petra', 'pokorna', 'premysl', 'radek', 'radim', 'radka', 'radko',
               'radmila', 'radomil', 'radomir', 'radoslav', 'radovan', 'renata', 'rene', 'richard', 'robert', 'robin',
               'rom', 'roman', 'romana', 'rostislav', 'rudolf', 'ruzena', 'sarka', 'simeon', 'slavomir', 'sona',
               'stanislav', 'stanislava', 'stefan', 'stepan', 'stepanka', 'svatava', 'svatopluk', 'svetlana', 'sylva',
               'tatana', 'tereza', 'tibor', 'tomas', 'tomio', 'vaclav', 'vaclava', 'vera', 'veronika', 'viera',
               'viktor', 'vilem', 'vit', 'vitezslav', 'vladimir', 'vladimira', 'vladislav', 'vladislava', 'vlasta',
               'vlastimil', 'vlastislav', 'vojtech', 'vratislav', 'yveta', 'yvona', 'zbynek', 'zdena', 'zdenek',
               'zdenka', 'zlatuse', 'zuzana', 'zuzka'}


def parse_speaker(speaker, log_prefix=''):
    """
Separates role and name from string like "Ministr zemědělsví ČR Augustin Karel Andrle Sylor"
    """
    if not speaker:
        return None

    tokens = speaker.split()
    # separates name and role
    SEP = None

    # heuristic - find fixed len titles
    sl = speaker.lower()
    for si, roles in fix_len_roles.items():
        if any(sl.startswith(t) for t in roles):
            SEP = si
            break

    # heuristic - find first given name in tokens
    for i, tok in enumerate(tokens):
        if normalize(tok) in given_names:
            if SEP is not None and SEP != i:
                stderr("%s: heuristic mismatch (%s != %s) for speaker '%s'" % (log_prefix, SEP, i, speaker))
                break
            SEP = i
            break

    # expect that name has length of 2 names
    if SEP is None:
        SEP = -2

    title = ' '.join((tokens[:SEP]))
    name = ' '.join(tokens[SEP:])
    name = re.sub(r'-', r' ', name)

    return name, title


def tag2str(tag):
    if tag is None:
        return None
    ts = tag.string
    if ts is None:
        return ''
    return ts.strip()


Row = namedtuple("Row", ['key', 'sid', 'basename', 'order', 'date', 'role', 'name', 'text'])