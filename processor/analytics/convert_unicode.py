#!/usr/bin/env python3

import json
import sys

if len(sys.argv) != 3:
    print("""usage: %s INPUT.json OUTPUT.json
converts INPUT.json to true utf-8, not the escaped version; writes output to OUTPUT.json"""%sys.argv[0])
    sys.exit(1)

with open(sys.argv[1], 'r') as fin:
    kws = json.load(fin)


def dumps_utf8(obj):
    return json.dumps(obj, ensure_ascii=False)


with open(sys.argv[2], 'wb') as fout:
    fout.write(dumps_utf8(kws).encode('utf-8'))
