from collections import defaultdict
from pprint import pprint  # pretty-printer

from gensim import corpora, models
from gensim.utils import deaccent, tokenize

import analyze_people


def normalize(word):
    word = word.strip().lower()
    return deaccent(word)


def load_stopwords():
    s = set()
    with open("stopwords", 'r') as fin:
        for line in fin:
            s.add(normalize(line))
    return s

def consequent_runs(iterator):
    prev = None
    current_group = []
    for row in iterator:
        current = row['name']

        if prev is None or current != prev:
            if not prev is None:
                yield current_group
                current_group = []

        current_group.append(row)
        prev = current

    if current_group:
        yield current_group

def first(grp):
    stopwords = set() #load_stopwords()
    rows = analyze_people.load('data/data_stage0_scraped_with_pos_tags_v0.jsonl', factory=lambda rowd : rowd)


    rows = [row for row in rows if row['sid'] != '1']

    by_name = defaultdict(lambda: list())
    original_words = {}

    #grp = True
    texts = []
    names = []
    for run in consequent_runs(rows):
        if not grp:
            l = []

        for row in run:
            if grp:
                l = by_name[row['name']]

            name = row['name']
            lemmas, tags = row['lemmas'], row['pos_tags']

            assert len(lemmas) == len(tags)
            for lemma, tag in zip(lemmas, tags):
                assert tag
                if tag[0] in ['N', 'A']: # ,'V']
                    lemma = lemma.split('-')[0]
                    lemma = lemma.split('`')[0]
                    l.append(lemma)

        if not grp:
            texts.append(l)
            names.append(name)

        if False:
            original_tokens = tokenize(row.text, lowercase=False, deacc=False)
            for token in original_tokens:
                tn = normalize(token)
                if tn in stopwords:
                    continue
                original_words[token] = tn
                l.append(tn)

    if grp:
        names = list(by_name.keys())
        texts = [by_name[name] for name in names]

    return original_words, names, texts


if __name__ == "__main__":
    original_words, names, texts = first()
    for t in texts:
        for w in t:
            print(w)

    if False:
        dictionary = corpora.Dictionary(texts)
        dictionary.save('corpora.dict')

        corpus = [dictionary.doc2bow(text) for text in texts]
        corpora.MmCorpus.serialize('corpora.mm', corpus)

        dictionary = corpora.Dictionary.load('corpora.dict')
        corpus = corpora.MmCorpus('corpora.mm')
        tfidf = models.TfidfModel(corpus)
        corpus_tfidf = tfidf[corpus]
        lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2)
        corpus_lsi = lsi[corpus_tfidf]
        lsi.print_topics(10)