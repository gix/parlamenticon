from datetime import datetime
from elasticsearch import Elasticsearch
import json
from collections import defaultdict, namedtuple
import sys
import re
import os

#elasticsearch_connection = os.getenv('ELASTICSEARCH_CONNECTION', 'http://localhost:9200')
#es = Elasticsearch([elasticsearch_connection])
#index_name = os.getenv('ELASTICSEARCH_INDEX', 'utterances')

dataset_id = 'psp'
dataset_prefix = "{}_".format(dataset_id)
elasticsearch_connection = os.getenv('ELASTICSEARCH_CONNECTION', 'http://localhost:9200')
es = Elasticsearch([elasticsearch_connection])
index_name = "{}{}utterances".format(dataset_prefix, os.getenv('ELASTICSEARCH_INDEX_PREFIX', ''))

print(sys.argv)
print("INDEX_NAME: ".format(index_name))
print("ELASTICSEARCH_CONNECTION: ".format(elasticsearch_connection))

num_of_keywords = 100

allowed_beginnings = ["N", "V", "A"]

script_name =  os.path.basename(sys.argv[0])

if len(sys.argv) != 4:
    print("Usage: {} source_file out_file_name.jsonl", sys.argv[0])
    print("python3.5 {} key ../../data/data_stage0_scraped_with_pos_tags_v0.jsonl ../../data/session_keywords_with_pos_tags_v1.json".format(script_name))
    sys.exit(1)

print(sys.argv)

forget, psp_key, source_file, out_file = sys.argv

print("Source: {}".format(source_file))
print("Out: {}".format(out_file))


Row = namedtuple("Row", ['sid', 'order', 'date', 'role', 'name', 'text', 'pos_tags', 'lemmas', 'tokens', 'key', 'basename'])

rows = []
sids = {}
with open(source_file, 'r') as fin:
    for i, row_js in enumerate(fin):
        rowd = json.loads(row_js.strip())
        rows.append(Row(**rowd))
        row = Row(**rowd)
        name = row.name
        sid = row.sid
        if sid not in sids:
            sids[sid] = {}

out = {}
out[psp_key] = {}

for sid in sids:
    body = {
        "query": {
            "term": {
                "sid": "{}".format(sid)
            }
        },
        "aggs": {
            "keywords": {
                "significant_text": {
                    "field": "text",
                    "gnd": {},
                    "size": num_of_keywords
                }
            }
        }
    }
    res = es.search(index=index_name, body=body)

    batch = {"keywords": [], "people_to_keywords": {}, "keywords_to_people": {}}
    order = 0
    #print(json.dumps(res['aggregations']))
    k_to_p = {}
    p_to_k = {}
    for bucket in res["aggregations"]["keywords"]["buckets"]:
        lemma = bucket["key"]
        k_to_p[lemma] = []

        subbody = {
            "query": {
                "bool": {
                "must": 
                [{
                "term": {
                    "sid": "{}".format(sid)
                }},
                {"match_phrase": {
                    "text": lemma
                }}]
                }
            },
            "aggs": {
                "people": {
                    "terms": {
                        "field": "name",
                        "size": 1000
                    }
                }
            }
        }
        subres = es.search(index=index_name, body=subbody)
        for subbucket in subres["aggregations"]["people"]["buckets"]:
            name = subbucket["key"]
            if name not in k_to_p[lemma]:
                k_to_p[lemma].append(name)

            if name not in p_to_k:
                p_to_k[name] = []
            if lemma not in p_to_k[name]:
                p_to_k[name].append(lemma)

        batch["keywords"].append({"text": lemma, "order": order, "score": bucket["score"], "doc_count": bucket["doc_count"], "bg_count": bucket["bg_count"], "people": k_to_p[lemma]})
        order += 1
        #print(batch)
        #print('\n')

    batch["people_to_keywords"] = p_to_k
    #sys.exit(0)    
    #batch["keywords_to_people"] = k_to_p

    out[psp_key][sid] = batch

with open(out_file, 'w', encoding="utf-8") as of:
    json.dump(out, of, sort_keys=True, indent=1, separators=(',', ': '))
    #str = json.dumps(out)
    #of.write(str)


#print("Got %d Hits:" % res['hits']['total'])
#for hit in res['hits']['hits']:
#    print("{}".format(hit))
