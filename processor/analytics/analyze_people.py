#!/usr/bin/env python3
import json
import sys
from collections import defaultdict, OrderedDict
from datetime import datetime

import numpy as np
from gensim.utils import tokenize

import utils


def translate_role(role):
    role = role.lower()
    tags = {}

    tags['is_mp'] = role.startswith('poslan')

    tags['is_minister'] = 'ministr' in role
    tags['is_senator'] = role.startswith('senátor')
    tags['is_czech_president'] = role == 'prezident české republiky'
    tags['is_moderator'] = ("předseda psp" in role) or role == "předsedající"
    tags['is_czech_cabinet_member'] = "předseda vlády čr" in role

    tags['is_guest'] = not (tags['is_mp'] or tags['is_moderator'])

    return tags


def one_stat(name, role, rows):
    assert all(row.role == role for row in rows)
    assert all(row.name == name for row in rows)

    d = OrderedDict()
    d['role_predicates'] = translate_role(role)
    d['name'] = name
    d['role'] = role

    total_tokens = []
    utterance_wc = []
    for current_run in utils.group_consequent_by_key(rows, lambda row: row.order):
        run_tokens = []
        for row in current_run:
            run_tokens.extend(tokenize(row.text, lowercase=True, deacc=True))
        utterance_wc.append(len(run_tokens))
        total_tokens.extend(run_tokens)
    uniq = set(total_tokens)
    d['utterance_count'] = len(utterance_wc)
    d['mean_word_count_per_utterance'] = np.mean(utterance_wc)
    d['median_word_count_per_utterance'] = np.median(utterance_wc)
    d['std_word_count_per_utterance'] = np.std(utterance_wc)
    d['word_count'] = len(total_tokens)
    d['unique_word_count'] = len(uniq)

    return d, utterance_wc, uniq


def parse_date(dt):
    return datetime.strptime(dt, "%Y-%m-%d")


if __name__ == "__main__":
    if len(sys.argv) <= 2:
        print("usage: %s DATA_DIR DATA_JSONL [DATA_JSONL ...]")
        print("parses jsonl datafiles and writes down statistics files")
        sys.exit(1)

    data_dir = sys.argv[1]
    files = sys.argv[2:]
    rows = list(utils.flatten(map(utils.load_jsonl, files)))

    print("loaded %d rows" % len(rows))

    # names
    if False:
        all_names = list(sorted(set([row.name for row in rows])))
        print("\n".join(all_names))

    # roles
    if False:
        all_roles = list(sorted(set([tuple(row.role.split(' ')) for row in rows]), key=lambda toks: (len(toks), toks)))
        print("\n".join(' '.join(r) for r in all_roles))

    # len by name
    if False:
        len_by_name = defaultdict(lambda: list())
        for row in rows:
            len_by_name[row.name].append(len(list(tokenize(row.text, lower=True, deacc=True))))

        SN = sorted([(sum(l), name) for name, l in len_by_name.items()])
        for total, name in SN:
            print("%4d\t%s" % (total, name))
            pass

    if True:
        by_keysid = defaultdict(lambda: defaultdict(lambda: list()))
        for row in rows:
            by_keysid[(row.key, row.sid)][(row.name, row.role)].append(row)
        sort_keysid = lambda t:(t[0], int(t[1]))

        if False:
            # prints MP that have two roles in one meeting
            for k in sorted(by_keysid.keys(), key=sort_keysid):
                d = by_keysid[k]
                prev = None
                for v in sorted(d.keys()):
                    if prev is not None and v[0] == prev[0]:
                        print(prev)
                        print(v)
                    prev = v

        stats_by_keysids = OrderedDict()
        by_keysids_date = OrderedDict()
        for k in sorted(by_keysid.keys(), key=sort_keysid):
            for (name, role), rows in sorted(by_keysid[k].items()):
                stat = one_stat(name, role, rows)
                stats_by_keysids.setdefault(k, list()).append(stat)
                by_keysids_date.setdefault(k, set()).update(set(row.date for row in rows))

            stats_by_keysids[k].sort(key=lambda t: - t[0]['word_count'])


        ret = OrderedDict()
        for (ps, sid), l in stats_by_keysids.items():
            ret.setdefault(ps, OrderedDict())[sid] = [d for d, _, _ in l]

        with open("{}/data_people_stats_by_sid_v2.json".format(data_dir), 'w') as fout:
            json.dump(ret, fout, indent=4)

        # compute length
        sidstats = OrderedDict()
        for k, dates in by_keysids_date.items():
            fr = min(dates)
            to = max(dates)

            delta = (parse_date(to) - parse_date(fr)).days
            sidstats[k] = {'date_from': fr,
                        'date_to': to,
                        'length_days': delta + 1}

        # add links
        for (key, sid), stats in stats_by_keysids.items():
            link = 'https://www.psp.cz/eknih/%s/stenprot/%s%sschuz/index.htm' % (key, '0' * (3 - len(sid)), sid)
            utterance_wc = list(utils.flatten(uwc for _, uwc, _ in stats))
            uniq = set(utils.flatten(u for _, _, u in stats))

            d = sidstats[(key, sid)]
            d['original_link'] = link
            d['utterance_count'] = len(utterance_wc)
            d['mean_word_count_per_utterance'] = np.mean(utterance_wc)
            d['median_word_count_per_utterance'] = np.median(utterance_wc)
            d['std_word_count_per_utterance'] = np.std(utterance_wc)
            d['word_count'] = sum(utterance_wc)
            d['unique_word_count'] = len(uniq)

        ret = OrderedDict()
        for (ps, sid), d in sidstats.items():
            ret.setdefault(ps, OrderedDict())[sid] = d

        with open("{}/data_sid_to_stats_v2.json".format(data_dir), 'w') as fout:
            json.dump(ret, fout, indent=4)

