import json
from collections import namedtuple, OrderedDict
from itertools import chain

from gensim.utils import deaccent

Row = namedtuple("Row", ['key', 'sid', 'basename', 'order', 'date', 'role', 'name', 'text'])


def flatten(iterable):
    return chain.from_iterable(iterable)


def load_jsonl(filename, factory=None):
    if factory is None:
        factory = lambda rowd: Row(**rowd)

    rows = []
    with open(filename, 'r') as fin:
        for i, row_js in enumerate(fin):
            rowd = json.loads(row_js.strip(), object_pairs_hook=OrderedDict)
            rows.append(factory(rowd))
    return rows


def stderr(*args, **kwargs):
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)


def key_uniq_name(row):
    return (row['key'], row['sid'], row['name'])


def group_consequent_by_key(iterator, key):
    prev_key = None
    current_group = []
    for row in iterator:
        current_key = key(row)
        if current_key != prev_key and prev_key is not None:
            yield current_group
            current_group = []

        current_group.append(row)
        prev_key = current_key

    if current_group:
        yield current_group


def normalize(word):
    word = word.strip().lower()
    return deaccent(word)


def load_stopwords(filename='stopwords'):
    s = set()
    with open(filename, 'r') as fin:
        for line in fin:
            s.add(normalize(line))
    return s


if __name__ == "__main__":
    print(list(group_consequent_by_key((1, 2, 2, 3, 3, 3), key=lambda t: t)))
