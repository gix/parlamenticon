#!/usr/bin/env bash

# first we try to connect to elasticsearch and fail if we can't - don't want to find out after 30min of scraping
# this is very handy for debugging of infrastructure
echo "Testing elasticsearch connection"
echo "  expected variable ELASTICSEARCH_CONNECTION=${ELASTICSEARCH_CONNECTION}"
curl -sf ${ELASTICSEARCH_CONNECTION} >/dev/null
if [[ $? != 0 ]]; then
    echo "Failed to connect to elasticsearch. Exiting with error status"
    exit 1
fi
echo "Elastic ok"



# and now for the actual processing
echo "Running makefile"
make all
if [[ $? != 0 ]]; then
    echo "Makefile failed. Exiting with error status"
    exit 1
fi
echo "Makefile passed!"