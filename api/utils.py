import collections
import json
import sys
from functools import wraps

from flask import Response
from werkzeug.exceptions import abort


def stderr(*args, **kwargs):
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)


def load_js(filename):
    with open(filename, 'r') as fin:
        str = fin.read()
        stderr("Read %d kiB from '%s'" % (len(str) // 1024, filename))
        return json.loads(str, object_pairs_hook=collections.OrderedDict)


def json_utf8_dump(obj):
        return json.dumps(obj, ensure_ascii=False)


def jsonify(obj, status=200):
    return Response(json.dumps(obj), status=status, mimetype='application/json')


def abort_on(exception, code):
    def deco(f):
        @wraps(f)
        def g(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exception:
                abort(code)

        return g

    return deco
