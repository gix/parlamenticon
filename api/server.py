#!/usr/bin/env python3
from flask import Flask, jsonify
from flask_cors import CORS
from elasticsearch import Elasticsearch

import os
import sys
import logging

from utils import abort_on

dataset_id = 'psp'
dataset_prefix = "{}_".format(dataset_id)
elasticsearch_connection = os.getenv('ELASTICSEARCH_CONNECTION', 'http://localhost:9200')
es = Elasticsearch([elasticsearch_connection])
index_name = "{}{}sessions".format(dataset_prefix, os.getenv('ELASTICSEARCH_INDEX_PREFIX', ''))

debug = True if (os.getenv('DEBUG', False) == 'True') else False

print(sys.argv)
print("INDEX_NAME: {}".format(index_name))
print("ELASTICSEARCH_CONNECTION: {}".format(elasticsearch_connection))

logging.basicConfig()

# FLASK GLOBALS
app = Flask(__name__)
api_prefix = '/api'
CORS(app, resources={r"/api/*": {"origins": "*"}})

# API routes

@app.route(api_prefix + '/health')
def health():
    return jsonify({'status': 'ok'})

#@app.route(api_prefix + '/keys')
#def api_keys():
#    data = list(db.session_stats.keys())
#    return jsonify(keys=data)


@app.route(api_prefix + '/parliaments/<key>/sessions')
@abort_on(KeyError, 404)
def api_sessions(key):
    query = {"term": {"dataset_id": key}}
    body = {"size": 1000, "query": query, "aggregations": {"last": {"max": {"field": "session_id"}}}}
    res = es.search(index=index_name, body=body, filter_path='**._id,**.session_id,aggregations')
    #print(res)
    assert ("aggregations" in res) and ('hits' in res) and ('hits' in res['hits'])
    last = int(res['aggregations']['last']['value'])
    sessions = [int(session['_source']['session_id']) for session in list(res['hits']['hits'])]
    return jsonify(key=key, sessions=sessions, last=last)

@app.route(api_prefix + '/parliaments/<key>/sessions/<session_id>/keywords')
@abort_on(KeyError, 404)
def api_sessions_keywords(key, session_id):
    query = {"bool": {"must": [{"term": {"dataset_id": key}}, {"term": {"session_id": session_id}}]}}
    body = {"size": 10, "query": query}
    res = es.search(index=index_name, body=body,filter_path='**._id,**.session_id,**.keywords,**.people_to_keywords')
    #print(res)
    assert ('hits' in res) and ('hits' in res['hits']) and (len(res['hits']['hits']) > 0)
    data = res['hits']['hits'][0]['_source']
    return jsonify(key=key, session=session_id, session_keywords=data["keywords"], people_to_keywords=data["people_to_keywords"])


@app.route(api_prefix + '/parliaments/<key>/sessions/<session_id>/session_stats')
@abort_on(KeyError, 404)
def api_session_stat(key, session_id):
    query = {"bool": {"must": [{"term": {"dataset_id": key}}, {"term": {"session_id": session_id}}]}}
    body = {"size": 1, "query": query}
    res = es.search(index=index_name, body=body,filter_path='**._id,**.session_id,**.stats')
    #print(res)
    assert ('hits' in res) and ('hits' in res['hits']) and (len(res['hits']['hits']) > 0)
    data = res['hits']['hits'][0]['_source']['stats']
    return jsonify(key=key, session=session_id, session_stats=data)


@app.route(api_prefix + '/parliaments/<key>/sessions/<session_id>/people_stats')
@abort_on(KeyError, 404)
def api_people_stat(key, session_id):
    query = {"bool": {"must": [{"term": {"dataset_id": key}}, {"term": {"session_id": session_id}}]}}
    body = {"size": 1, "query": query}
    res = es.search(index=index_name, body=body,filter_path='**._id,**.session_id,**.people_stats')
    #print(res)
    assert ('hits' in res) and ('hits' in res['hits']) and (len(res['hits']['hits']) > 0)
    data = res['hits']['hits'][0]['_source']['people_stats']
    return jsonify(key=key, session=session_id, people_stats=data)


if __name__ == '__main__':
    app.run(debug=debug, threaded=True, host='0.0.0.0')
